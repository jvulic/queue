package queue

import "sync"

/*
	A simple wrapper on any Queue that synchronizes access by means applying a
	RWMutex to all reads and writes.
*/

type safeQueue struct {
	sync.RWMutex
	in Queue
}

func safe(d Queue) Queue {
	return &safeQueue{in: d}
}

func (d *safeQueue) Length() int {
	d.RLock()
	defer d.RUnlock()
	return d.in.Length()
}

func (d *safeQueue) Capacity() int {
	d.RLock()
	defer d.RUnlock()
	return d.in.Capacity()
}

func (d *safeQueue) Front() interface{} {
	d.RLock()
	defer d.RUnlock()
	return d.in.Front()
}

func (d *safeQueue) Push(elem interface{}) {
	d.Lock()
	defer d.Unlock()
	d.in.Push(elem)
}

func (d *safeQueue) Pop() {
	d.Lock()
	defer d.Unlock()
	d.in.Pop()
}
