package queue

import (
	"bitbucket.org/jvulic/deque"
)

type dequeQueue struct {
	d deque.Deque
}

// NewDQ creates the default deque-based queue.
func NewDQ() Queue {
	// The default deque backend uses the ring-buffer implementation as it has
	// demonstrated better performance under benchmarking.
	return &dequeQueue{d: deque.NewRD()}
}

// NewDQSelect creates a new queue with the given deque as the backend.
func NewDQSelect(deq deque.Deque) Queue {
	return &dequeQueue{d: deq}
}

func (q *dequeQueue) Length() int {
	return q.d.Length()
}

func (q *dequeQueue) Capacity() int {
	return q.d.Capacity()
}

func (q *dequeQueue) Front() interface{} {
	return q.d.Front()
}

func (q *dequeQueue) Push(elem interface{}) {
	q.d.PushBack(elem)
}

func (q *dequeQueue) Pop() {
	q.d.PopFront()
}
