/*
Package queue provides a flexible implemenation of the queue ADT.
*/
package queue

// Queue defines the queue ADT.
type Queue interface {
	// Returns the current length of the queue.
	Length() int

	// Returns the current capacity of the queue. Returns -1 for inf capacity.
	Capacity() int

	// Returns the element at the head of the queue; does NOT remove it. Panics
	// if the queue is empty.
	Front() interface{}

	// Adds an element to the head of the queue.
	Push(elem interface{})

	// Removes the element at the head of the queue. Panics if the queue is empty.
	Pop()
}

type options struct {
	backend Queue
	safe    bool
}

// Option configures aspects of the queue.
type Option func(*options)

// Safe returns an Option that synchronizes access to the underlying deque
// implementation.
func Safe() Option {
	return func(o *options) {
		o.safe = true
	}
}

// Backend returns an Option that sets the implemenation of the underlying
// queue.
func Backend(queue Queue) Option {
	return func(o *options) {
		o.backend = queue
	}
}

// New creates a new queue.
func New(opt ...Option) Queue {
	var opts options
	for _, o := range opt {
		o(&opts)
	}

	// Sets the default backend if unspecified.
	if opts.backend == nil {
		opts.backend = NewDQ()
	}

	if opts.safe {
		return safe(opts.backend)
	}
	return opts.backend
}
