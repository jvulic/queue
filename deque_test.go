package queue

import (
	"math/rand"
	"testing"

	"bitbucket.org/jvulic/deque"
)

func TestQueueBasic(t *testing.T) {
	// Correctness is already verified by the underlying queue library used.
}

// A simple benchmarking function that performs all the possible mutating
// actions on a queue.
func benchmarkMixed(queue Queue, seed int64, iters int) {
	r := rand.New(rand.NewSource(seed))
	for i := 0; i < iters; i++ {
		// Randomly select one of the four operations to perform.
		switch r.Int() % 2 {
		case 0:
			queue.Push(i)
		case 1:
			if queue.Length() <= 0 {
				queue.Push(i)
				continue
			}
			queue.Pop()
		}
	}
}

func BenchmarkListQueue(b *testing.B) {
	queue := NewDQSelect(deque.NewLD())
	b.ResetTimer()
	benchmarkMixed(queue, 0, b.N)
}

func BenchmarkListQueueSafe(b *testing.B) {
	queue := safe(NewDQSelect(deque.NewLD()))
	b.ResetTimer()
	benchmarkMixed(queue, 0, b.N)
}

func BenchmarkRingQueue(b *testing.B) {
	queue := NewDQSelect(deque.NewRD())
	b.ResetTimer()
	benchmarkMixed(queue, 0, b.N)
}

func BenchmarkRingQueueSafe(b *testing.B) {
	queue := safe(NewDQSelect(deque.NewRD()))
	b.ResetTimer()
	benchmarkMixed(queue, 0, b.N)
}

func BenchmarkSliceQueue(b *testing.B) {
	queue := NewDQSelect(deque.NewSD())
	b.ResetTimer()
	benchmarkMixed(queue, 0, b.N)
}

func BenchmarkSliceQueueSafe(b *testing.B) {
	queue := safe(NewDQSelect(deque.NewSD()))
	b.ResetTimer()
	benchmarkMixed(queue, 0, b.N)
}
